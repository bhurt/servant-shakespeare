# servant-shakespeare

## Justification

The idea for this module started with a desire for a "Yesod-lite" library.
Yesod has gotten pretty heavy weight over time.  There are cases where
that is a good thing- if you're doing a seriously large website, you're
going to need most of the stuff Yesod provides.  But if you're only
doing a small website, then Yesod starts to be cumbersome.  This is
especially true if you don't agree with Yesod's opinions, for example
if you don't want to use it's choosen logging library or choosen
database access library.

So the idea was to pull out those parts of Yesod which are core to why
Yesod is so nice, into a lighter-weight, less opinionated, library.
These are:
- The Shakespearean templates
- Type-safe URLs
- Widgets
- Form security

This raises the question: how to implement the router?  The answer was
to use Servant's.  Bring the best parts of Yesod to the Servant ecosystem.
